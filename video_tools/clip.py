from moviepy.video.io.VideoFileClip import VideoFileClip

folder = "C:\\Users\\Nathan\\Documents\\Bandicam\\"
original_clip = "your_clip.mp4"
output_file_name = "out.mp4"

input_video_path = folder + original_clip
output_video_path = folder + output_file_name

t1 = 1
t2 = 29
with VideoFileClip(input_video_path) as video:
    new = video.subclip(t1, t2)
    new.write_videofile(output_video_path, audio_codec='aac')