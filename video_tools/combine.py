from moviepy.editor import VideoFileClip, concatenate_videoclips

folder = "C:your\\folder\\"
original_clip_1 = "your_clip.mp4"
original_clip_2 = "your_clip.mp4"
output_file_name = "out.mp4"

clip1 = VideoFileClip(folder + original_clip_1)
clip2 = VideoFileClip(folder + original_clip_2)

final = concatenate_videoclips([clip1, clip2])
final.write_videofile(folder + output_file_name)