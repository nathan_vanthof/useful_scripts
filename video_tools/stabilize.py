from video_tools.utils.stabilize import straighten_vid
import os

folder = "C:your\\folder\\"
original_clip = "your_clip.mp4"
output_file_name = "out.mp4"


straighten_vid(input_name=folder + original_clip,
               output_name=folder + output_file_name)
